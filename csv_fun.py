#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# csv fun - Very Basic csc importing
##############################################################################

import inkex

from inkex import command, Circle, Rectangle, units, styles, Group
from lxml import etree
import tempfile, os, shutil, random
from pathlib import Path

import csv


def get_attributes(self):
    for att in dir(self):
        inkex.errormsg((att, getattr(self, att)))


def group_wrapper(self, my_object, group_id):
    if self.svg.getElementById('g' + group_id) is not None:
        new_group = self.svg.getElementById('g' + group_id)

    else:
        group_id = 'g' + group_id

        new_group = self.svg.get_current_layer().add(Group.new('#' + group_id))
        new_group.set('inkscape:groupmode', 'layer')
        new_group.attrib['id'] = group_id
    new_group.append(my_object)

    return group_id


def create_new_group(self, prefix):
    group_id = str(prefix) + '_' + str(random.randrange(100000, 999999))

    # new_group = self.svg.get_current_layer().add(Group.new('#' + group_id))

    new_group = self.svg.add(Group.new('#' + group_id))

    new_group.set('inkscape:groupmode', 'layer')
    new_group.attrib['id'] = group_id

    return new_group


def import_csv(self):
    csv_filepath = self.options.csv_filepath
    csv_list = []
    # csv_row = []
    try:
        with open(csv_filepath, newline='') as csvfile:

            my_csv_data = csv.reader(csvfile, delimiter=',', quotechar='"')

            for row in my_csv_data:
                csv_row = []
                # inkex.errormsg(f'row length {len(row)}')
                for column in row:
                    if not column:
                        column = '#null'

                    csv_row.append(column)

                csv_list.append(csv_row)
            
    except:
        None

    # inkex.errormsg(csv_list)
    return csv_list


def make_temp_svg(ink_svg_path, temp_filename):
    temp_dir = tempfile.gettempdir()
    temp_filename_path = os.path.join(temp_dir, temp_filename)
    shutil.copy2(ink_svg_path, temp_filename_path)
    return temp_filename_path


def pixel_convert_units(self, value):
    conversions = {
        'in': 96.0,
        'pt': 1.3333333333333333,
        'px': 1.0,
        'mm': 3.779527559055118,
        'cm': 37.79527559055118,
        'm': 3779.527559055118,
        'km': 3779527.559055118,
        'Q': 0.94488188976378,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
        '': 1.0,  # Default px
    }

    found_units = self.svg.unit

    try:
        pixel_conversion_factor = conversions[found_units]
        return value / pixel_conversion_factor
    except:
        pixel_conversion_factor = 1
        return value


def query_all_bbox(self):
    my_file_path = self.options.input_file

    with tempfile.NamedTemporaryFile(mode='r+', suffix='.svg') as temp_svg_file:
        # Write the contents of the updated svg to a tempfile to use with command line
        my_svg_string = self.svg.root.tostring().decode("utf-8")
        temp_svg_file.write(my_svg_string)
        temp_svg_file.read()
        my_query = inkex.command.inkscape(temp_svg_file.name, '--query-all')
        my_query_items = my_query.split('\n')

        my_element_list = []
        for my_query_item in my_query_items:

            my_element = my_query_item.split(',')

            # Reject any malformed query lines
            if len(my_element) > 4 and ('tspan' in my_element[0] or 'text' in my_element[0]):
                my_element_list.append(my_element)

        # Add Bounding box info to objects in current SVG.

        for element in my_element_list:

            my_element_object = self.svg.getElementById(element[0])

            if 'tspan' in element[0]:
                object_type = 'tspan'
            else:
                object_type = 'text'

            cl_bbox_x = float(element[1])
            cl_bbox_y = float(element[2])
            cl_bbox_width = float(element[3])
            cl_bbox_height = float(element[4])
            cl_bbox_center_x = (cl_bbox_x + (cl_bbox_x + cl_bbox_width)) / 2
            cl_bbox_center_y = (cl_bbox_y + (cl_bbox_y + cl_bbox_height)) / 2
            my_element_object.cl_bbox_x = cl_bbox_x
            my_element_object.cl_bbox_y = cl_bbox_y
            my_element_object.cl_bbox_width = cl_bbox_width
            my_element_object.cl_bbox_height = cl_bbox_height
            my_element_object.cl_bbox_center_x = cl_bbox_center_x
            my_element_object.cl_bbox_center_y = cl_bbox_center_y

            # my_element_object.attrib['cl_bbox_width'] = str(cl_bbox_width)
            # my_element_object.attrib['cl_bbox_height'] = str(cl_bbox_height)
            # my_element_object.attrib['cl_bbox_center_x'] = str(cl_bbox_center_x)
            # my_element_object.attrib['cl_bbox_center_y'] = str(cl_bbox_center_y)
            # my_element_object.attrib['cl_bbox_x'] = str(cl_bbox_x)
            # my_element_object.attrib['cl_bbox_y'] = str(cl_bbox_y)


def list_to_text_objects(self, text_list):
    text_id_prefix = random.randrange(100000, 999999)

    # parent = self.svg.get_current_layer()

    parent = create_new_group(self, 'csv_text')

    text_list_with_objects = []

    row_number = 0

    for row in text_list:
        column_number = 0
        text_list_row = []
        # loop across column cells in row
        for current_cell in row:
            text_object = etree.SubElement(parent, inkex.addNS('text', 'svg'))

            text_object.text = current_cell

            text_object_id = 'text_col_' + str(column_number) + '_row_' + str(row_number) + '_' + str(text_id_prefix)
            text_object.attrib['id'] = text_object_id
            text_object.style['font-size'] = str(self.options.font_size)

            text_object.attrib['data-colno'] = str(column_number)
            text_object.attrib['data-rowno'] = str(row_number)

            text_list_row.append([current_cell, text_object])

            column_number += 1
        text_list_with_objects.append(text_list_row)

        row_number += 1

    return text_list_with_objects


def arrange_text_objects(self, text_list_with_objects):
    # Get the max width for each column
    row_number = 0
    row_max_height_list = [0] * len(text_list_with_objects)
    column_max_width_list = [0] * len(text_list_with_objects[0])

    row_spacer = self.options.row_spacing
    column_spacer = self.options.column_spacing

    for row in text_list_with_objects:

        column_number = 0
        for column_cell in row:
            if column_cell[1].cl_bbox_width > column_max_width_list[column_number]:
                column_width = column_cell[1].cl_bbox_width
                column_max_width_list[column_number] = column_width
            # inkex.errormsg(column_cell[1].cl_bbox_height)
            if column_cell[1].cl_bbox_height > row_max_height_list[row_number]:
                row_height = column_cell[1].cl_bbox_height
                row_max_height_list[row_number] = row_height

            column_number += 1
        row_number += 1

    # Now we have the column width list, offset n+1 column by max width of n column
    row_number = 0
    cumulative_row_y = 0

    for row in text_list_with_objects:
        column_number = 0
        cumulative_column_x = 0
        for column_cell in row:

            current_cell_id = column_cell[1].eid
            current_element = self.svg.getElementById(current_cell_id)
            if column_number > 0:
                current_element.attrib['x'] = str(pixel_convert_units(self, cumulative_column_x))

            cumulative_column_x += column_max_width_list[column_number]
            cumulative_column_x += column_spacer
            current_element.attrib['y'] = str(pixel_convert_units(self, cumulative_row_y))
            column_number += 1
            # inkex.errormsg(cumulative_column_x)

        cumulative_row_y += row_max_height_list[row_number]
        cumulative_row_y += row_spacer

        row_number += 1


def draw_cell_borders(self, text_list_with_objects):
    new_group = create_new_group(self, 'bounding_boxes')

    frame_margin = self.options.bounding_boxes_margin

    for row in text_list_with_objects:
        for item in row:
            rectangle_x = pixel_convert_units(self, item[1].cl_bbox_x - frame_margin / 2)
            rectangle_y = pixel_convert_units(self, item[1].cl_bbox_y - frame_margin / 2)
            rectangle_width = pixel_convert_units(self, item[1].cl_bbox_width + frame_margin)
            rectangle_height = pixel_convert_units(self, item[1].cl_bbox_height + frame_margin)

            my_rectangle = Rectangle().new(rectangle_x, rectangle_y, rectangle_width, rectangle_height)
            my_rectangle.style['stroke-width'] = 1
            my_rectangle.style['stroke'] = 'green'
            my_rectangle.style['fill'] = 'none'

            new_group.append(my_rectangle)

            # self.svg.get_current_layer().append(my_rectangle)


class CsvFun(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--csv_filepath", type=str, dest="csv_filepath", default=None)

        pars.add_argument("--font_size", type=int, dest="font_size", default=20)

        pars.add_argument("--row_spacing", type=int, dest="row_spacing", default=20)

        pars.add_argument("--column_spacing", type=int, dest="column_spacing", default=20)

        pars.add_argument("--bounding_boxes", type=str, dest="bounding_boxes")

        pars.add_argument("--bounding_boxes_margin", type=int, dest="bounding_boxes_margin", default=20)

    def effect(self):

        # Check for csv file path
        if Path(self.options.csv_filepath).is_file():
            None
        else:
            inkex.errormsg('Please Select a CSV File')
            return

        my_text_list = import_csv(self)

        text_list_with_objects = list_to_text_objects(self, my_text_list)

        # Add all bounding box values to objects
        # This is not done in the loop so we only need 1 command line call.
        query_all_bbox(self)

        # Now All objects should have bounding box values attached we can arrange them

        arrange_text_objects(self, text_list_with_objects)

        # Get bboxes again after arrangement !
        if self.options.bounding_boxes == 'true':
            query_all_bbox(self)
            draw_cell_borders(self, text_list_with_objects)


if __name__ == '__main__':
    CsvFun().run()
